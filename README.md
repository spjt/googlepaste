# googlepaste
Clean up google url's

A simple self-contained webpage that has two input boxes and a button. Paste a URL copied from a google
search into the first box and the actual URL will appear in the second one. Click "select" to select
the contents of the second box.

e.g. it turns 

https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiXn460l8bLAhXMLSYKHTnqCmIQFggcMAA&url=https%3A%2F%2Fgithub.com%2F&usg=AFQjCNH3ZL3XV5BXG7wkswWu5GxjTFtoJg&sig2=BUoLF1mboXz7XsAqnixKyw&bvm=bv.116954456,d.eWE

into

https://github.com/

I made this because there was basically no way to copy a URL out of a Google search result on a phone.
